Pod::Spec.new do |s|
  s.name         = "UIView"
  s.version      = "0.0.1"
  s.summary      = "UIView 的一系列方便的Category."
  s.homepage     = "https://bitbucket.org/whirlwind/uiview"
  s.license      = 'MIT'
  s.author       = { "Whirlwind" => "whirlwindjames@foxmail.com" }
  s.source       = { :git => "https://bitbucket.org/whirlwind/uiview.git", :tag => 'v0.0.1' }
  s.platform     = :ios
  s.source_files = '*.{h,m}'
  s.requires_arc = false
end
