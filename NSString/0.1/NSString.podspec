Pod::Spec.new do |s|
  s.name         = "NSString"
  s.version      = "0.1"
  s.summary      = "NSString 的一系列方便的Category."
  s.homepage     = "https://bitbucket.org/whirlwind/nsstring"
  s.license      = 'MIT'
  s.author       = { "Whirlwind" => "whirlwindjames@foxmail.com" }
  s.source       = { :git => "https://bitbucket.org/whirlwind/nsstring.git", :tag=>'v0.1'}
  s.platform     = :ios
  s.source_files = '*.{h,m}'
  s.requires_arc = false
end
