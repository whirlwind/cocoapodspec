Pod::Spec.new do |s|
  s.name         = "NSUserDefaults+AppVersion"
  s.version      = "0.0.1"
  s.summary      = "app版本控制组件."
  s.homepage     = "https://bitbucket.org/whirlwind/nsuserdefaults-appversion"
  s.license      = 'MIT'
  s.author       = { "Whirlwind" => "whirlwindjames@foxmail.com" }
  s.source       = { :git => "https://bitbucket.org/whirlwind/nsuserdefaults-appversion.git", :tag => 'v0.0.1'}
  s.platform     = :ios
  s.source_files = '*.{h,m}'
  s.requires_arc = false
end
