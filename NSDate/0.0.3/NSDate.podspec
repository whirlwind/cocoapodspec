Pod::Spec.new do |s|
  s.name         = "NSDate"
  s.version      = "0.0.3"
  s.summary      = "some categories of NSDate."
  s.homepage     = "https://bitbucket.org/whirlwind/nsdate"
  s.license      = 'MIT'
  s.author       = { "Whirlwind" => "whirlwindjames@foxmail.com" }
  s.source       = { :git => "https://bitbucket.org/whirlwind/nsdate.git", :tag => "v0.0.3" }
  s.platform     = :ios
  s.source_files = '*.{h,m}'
  s.requires_arc = false
end
