Pod::Spec.new do |s|
  s.name         = "JSONAPI"
  s.version      = "1.1"
  s.summary      = "JSONKit的一种间接调用，转换SBJSON调用到JSONKit."
  s.homepage     = "https://bitbucket.org/whirlwind/jsonapi"
  s.license      = 'MIT'
  s.author       = { "Whirlwind" => "WhirlwindJames@foxmail.com" }
  s.source       = { :git => "https://bitbucket.org/whirlwind/jsonapi.git", :tag => 'v1.1'}
  s.platform     = :ios
  s.source_files = '*.{h,m}'
  s.dependency 'JSONKit'
  s.requires_arc = false
end
