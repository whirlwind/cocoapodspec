Pod::Spec.new do |s|
  s.name         = 'VersionString'
  s.version      = '0.1'
  s.license      = 'BSD'
  s.homepage     = 'https://bitbucket.org/whirlwind/versionstring'
  s.authors      = { 'Whirlwind' => 'james@boohee.com' }
  s.summary      = '一种比较版本号字符串的模块.'
  s.source       = { :git => 'https://bitbucket.org/whirlwind/versionstring.git', :tag => 'v0.1' }
  s.source_files = '*.{h,m}'
  s.requires_arc = false
end
