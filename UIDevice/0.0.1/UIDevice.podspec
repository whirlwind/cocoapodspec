Pod::Spec.new do |s|
  s.name         = "UIDevice"
  s.version      = "0.0.1"
  s.summary      = "UIDevice 的一系列方便的Category."
  s.homepage     = "https://bitbucket.org/whirlwind/uidevice"
  s.license      = 'MIT'
  s.author       = { "Whirlwind" => "whirlwindjames@foxmail.com" }
  s.source       = { :git => "https://bitbucket.org/whirlwind/uidevice.git" , :tag=>'v0.0.1'}
  s.platform     = :ios
  s.source_files = '*.{h,m}'
  s.dependency 'NSString', '~>0.1'
  s.requires_arc = false
end
